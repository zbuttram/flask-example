# Flask Example

## Running the Example

The default database is a Postgres database running on localhost named 'flask-example'.

To create the default database, install Postgres and run: `createdb flask-example`

To run the example, use the following commands:

```
pipenv install
pipenv run python mock_db.py
export FLASK_APP=main.py
export FLASK_ENV=development
pipenv run flask run
```

In Windows, substitute `set` for `export`.

If you are missing `pipenv`, go here: https://docs.pipenv.org/

If you want to use a custom database, just configure your DATABASE_URL in the .env file.
For more info on database urls, see this doc: http://docs.sqlalchemy.org/en/latest/core/engines.html#database-urls
The example is tested on Postgres but an attempt has been made to keep queries database-agnostic.
