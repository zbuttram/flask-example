"""
    Import the dependencies we need to run the app.
"""
# We'll be using python-dotenv to load environment variables from our .env file
from dotenv import load_dotenv

#  We'll need os from the Python standard library to read environment variables
import os

# Flask is of course our server framework, and we'll also need some utilities to use in our routes
from flask import Flask, render_template, request

# Records is our database library
import records

# Load environment variables from './.env' into our environment
load_dotenv()

# initialize the Flask app
app = Flask(__name__)

# fetch the DATABASE_URL environment variable
DATABASE_URL = os.getenv("DATABASE_URL")

# initialize our database connection
db = records.Database(DATABASE_URL)

# this function uses our database connection to query all columns ('*') from everything in the table 'chips'
def get_all_chips():
    return db.query("select * from chips;")


# this function will search our chips table for rows with a name matching the query
# the 'wildcard' parameter here defualts to True if it is not provided
def search_chips(query, wildcard=True):
    if wildcard:
        # if wildcard is true, add wildcards to the ends of the query string
        query = "%" + query + "%"
    # run our search query, uppercasing both the names in the database and our query so our search is case-insensitive
    # Records fills in :name with the what was given to the name= parameter
    return db.query(
        "select * from chips where upper(name) LIKE :name;", name=query.upper()
    )
    # Note: there are better ways to do case-insensitivity in Postgres, but we're trying to be database-agnostic here


# decorate the following function to be a route at "/" in our Flask server
@app.route("/")
def chips_index():
    # initialize our chips variable with an empty list
    chips = []

    # attempt to pull a URL parameter named 'query' from the current request
    query = request.args.get("query")

    # if we found a query in the URL
    if query:
        # set chips to be a search result
        chips = search_chips(query)
    else:
        # otherwise, just get all of them
        chips = get_all_chips()

    # render our index template, passing it our chips variable
    return render_template("index.html", chips=chips)

